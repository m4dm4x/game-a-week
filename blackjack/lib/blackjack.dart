import 'dart:math';

enum BlackJackResult {
  playing,
  lost,
  draw,
  win,
  blackJack;
}

const deckSize = 52;

String cardColor(int id) =>
  switch(id ~/ 13) {
    0 => 's', // spades
    1 => 'h', // hearts
    2 => 'd', // diamonds
    3 => 'c', // clubs
    _ => throw ArgumentError.value(id)
  };

String cardName(int id) =>
  switch(id % 13) {
    > 0 && < 10 => (id % 13 + 1).toString(),
    10 => 'j', // Jack
    11 => 'q', // Queen
    12 => 'k', // King
     0 => 'a', // Ace
     _ => throw ArgumentError.value(id)
  };

int _cardValue(int id) {
  switch(id % 13) {
    case 0: return 11;
    case > 0 && <10: return (id % 13 + 1);
    case 10: return 10;
    case 11: return 10;
    case 12: return 10;
    default: throw ArgumentError.value(id);
  }
}

int _handValue(List<int> hand) {
  int value = 0;
  int aces = 0;
  for (final i in hand) {
    final cvalue = _cardValue(i);
    if (11 == cvalue) { ++aces; }
    value += cvalue;
  }
  for (int i = 0; value > 21 && i < aces; ++i) {
    value -= 10;
  }
  return value;
}

class BlackJack {
  BlackJack([int deckCount = 1]) : _deck = List<int>.generate(deckCount * deckSize, (i) => i % deckSize) {
    assert(deckCount > 0);
    _deck.shuffle();
  }

  final List<int> _deck;
  int _cardIndex = 0;

  bool playing = false;
  final List<int> _dealer = [];
  final List<int> _player = [];

  List<int> get dealerHand => playing ? _dealer.sublist(1,2) : _dealer;
  List<int> get playerHand => _player;

  int get playerScore => _handValue(playerHand);
  int get dealerScore => _handValue(dealerHand);
  
  int nextCard() {
    final card = _deck[_cardIndex];
    ++_cardIndex;
    if (_cardIndex == _deck.length) {
      _deck.shuffle();
      _cardIndex = 0;
    }
    return card;
  }

  void startGame() {
    //_cardIndex = 0;
    //_deck.shuffle();
    playing = true;
    _dealer.clear();
    _player.clear();
    for (int i = 0; i < 2; ++i) {
      _dealerHit();
      playerHit();
    }
  }

  void _dealerHit() {
    _dealer.add(nextCard());
  }

  void playerHit() {
    _player.add(nextCard());
    if (playerScore >= 21) {
      playing = false;
    }
  }

  void playerStay() {
    if (!playing) {
      return;
    }
    playing = false;
    while (dealerScore < 17) {
      _dealerHit();
    }
    
  }

  BlackJackResult result() {
    if (playing) {
      return BlackJackResult.playing;
    } else if (playerScore > 21) {
      return BlackJackResult.lost;
    } else if (playerScore == dealerScore) {
      return BlackJackResult.draw;
    } else if (playerScore == 21) {
      return BlackJackResult.blackJack;
    } else if (dealerScore < playerScore ) {
      return BlackJackResult.win;
    } else if (dealerScore > 21) {
      return BlackJackResult.win;
    } else {
      return BlackJackResult.lost;
    }
  }
}

class ResultCounter {
  int win = 0;
  int lost = 0;
  int draw = 0;
  void add(BlackJackResult result) {
    switch(result) {
      case BlackJackResult.lost: ++lost;
      case BlackJackResult.win: ++win;
      case BlackJackResult.blackJack: ++win;
      case BlackJackResult.draw: ++draw;
      case BlackJackResult.playing: /* no-op */
    }
  }
}

String resultString(BlackJackResult result) =>
  switch(result) {
    BlackJackResult.lost => '🃏 Dealer wins!',
    BlackJackResult.win => '🏆 Player wins!',
    BlackJackResult.blackJack => '🏆 Player wins!',
    BlackJackResult.draw => '🫥 No winner...',
    BlackJackResult.playing => ''
  };


/*
void main(List<String> arguments) {
  final blackJack = BlackJack();
  int winCount = 0;
  int lostCount = 0;
  int drawCount = 0;
  
  while (true) {
   blackJack.startGame();
    while (BlackJackResult.playing == blackJack.result()) {
      printHand('\nDealer', blackJack.dealerHand);
      printHand('Player', blackJack.playerHand);
      print('(h)it (s)tay (q)uit');
      switch (stdin.readLineSync()) {
        case null: continue;
        case 'q': exit(0);
        case 'h': blackJack.playerHit();
        case 's': blackJack.playerStay();
      }
    }
    switch(blackJack.result()) {
      case BlackJackResult.blackJack: ++winCount;
      case BlackJackResult.win: ++winCount;
      case BlackJackResult.lost: ++lostCount;
      case BlackJackResult.draw: ++drawCount;
      default: print('UNSUPPORTED');
    }
    printHand('Dealer', blackJack.dealerHand);
    printHand('Player', blackJack.playerHand);
    print('\n\t${blackJack.result().toString().toUpperCase()}\n\tWin: $winCount Lost: $lostCount Draw: $drawCount\n');
  }
}
*/
  /*
  if ('d' == value) {
    if (2 == player.length && 8 < playerValue && playerValue < 12) {
    print('<< DOUBLE >>');
    decIterator.moveNext();
    player.add(decIterator.current);
    playerValue = handValue(player);
    break;
    }
 */