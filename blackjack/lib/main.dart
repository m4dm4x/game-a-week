import 'package:blackjack/blackjack.dart';
import 'package:flutter/material.dart';


void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        brightness: Brightness.dark,
//        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepOrange),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class Card extends StatelessWidget {
  const Card(this.id, {super.key});

  final int id;

  String get _color => cardColor(id);
  String get _name => cardName(id);
  String get _image => 'images/card_b_$_color$_name.png';

  @override
  Widget build(BuildContext context) {
    return Padding(padding: EdgeInsets.fromLTRB(5,5,5,5), child:
    Image.asset(id >= 0 ? _image : 'images/deck_3.png', scale: 1.2,));
  }

}

class CardList extends StatelessWidget {
  const CardList(this.cards, {super.key});

  final List<int> cards;

  @override
  Widget build(BuildContext context) =>
    Stack(
      children: List<Widget>.generate(cards.length, (index) =>
        Padding(
          padding: EdgeInsets.fromLTRB(70.0*index,5.0*index,0,0),
          child: Container(
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: const Color.fromARGB(255, 3, 3, 3).withOpacity(0.5),
                    spreadRadius: 5,
                    blurRadius: 10,
                    //offset: Offset(0, 0), // changes position of shadow
                  ),
                ],
              ),
            child: Card(cards[index]),
          )
        )
      )
    );    
}

class _MyHomePageState extends State<MyHomePage> {
  final blackJack = BlackJack();
  final counter = ResultCounter();

  @override
  void initState() { 
    super.initState();
  }

  void _tap() => setState((){
    blackJack.playerHit();
    counter.add(blackJack.result());
  });

  void _stay() => setState((){
    blackJack.playerStay();
    counter.add(blackJack.result());
  });

  void _newGame() => setState((){
    blackJack.startGame();
    counter.add(blackJack.result());
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff212a31),
/*
      appBar: AppBar(
        backgroundColor: const Color(0xff212a31),
        title: const Text('BlackJack'),
      ),
*/
      body: 
      Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(resultString(blackJack.result()), style: TextStyle(fontSize: 30),),
            Text(blackJack.dealerScore.toString()),
            CardList(blackJack.dealerHand.length < 2 ? [-1, ...blackJack.dealerHand] : blackJack.dealerHand),
            CardList(blackJack.playerHand),
          Text(blackJack.playerScore.toString()),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
        children: [
          if (BlackJackResult.playing != blackJack.result()) ElevatedButton(onPressed: _newGame, child: Text('Deal')),
          if (BlackJackResult.playing == blackJack.result()) OutlinedButton(onPressed: _tap, child: Text('Hit')),
          if (BlackJackResult.playing == blackJack.result()) const SizedBox(width: 10,),
          if (BlackJackResult.playing == blackJack.result()) OutlinedButton(onPressed: _stay, child: Text('Stay')),
        ],
      ),
        ],),
      ),
      bottomNavigationBar: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text('Win: ${counter.win}'),
          Text('Lost: ${counter.lost}'),
          Text('Draw: ${counter.draw}'),
        ],
      )
    );
  }
}
