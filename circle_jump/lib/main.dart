import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:math' as math;

import 'package:flutter/services.dart';

void main() {
  runApp(const CircleJumpApp());
}

class CircleJumpApp extends StatelessWidget {
  const CircleJumpApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Circle Jump',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepOrange),
        useMaterial3: true,
      ),
      home: const GamePage(),
    );
  }
}

class GamePage extends StatefulWidget {
  const GamePage({super.key});

  @override
  State<GamePage> createState() => _GamePageState();
}

class CircleObject {
  CircleObject({required this.base, required this.angle, required this.height, required this.width});
  
  double base;
  double angle;
  double height;
  double width;

  double get top => base + height;

  double get x => (base + (height / 2)) * math.cos(angle);
  double get y => (base + (height / 2)) * math.sin(angle);

  bool intersects(CircleObject o) {
    if (top < o.base || o.top < base ) {
      return false;
    }
    final cbase = math.max(base, o.base);

    final aAngle = math.atan((width / 2) / cbase);
    final aStart = angle - aAngle;
    final aEnd = angle + aAngle;

    final bAngle = math.atan((o.width / 2) / cbase);
    final bStart = o.angle - bAngle;
    final bEnd = o.angle + bAngle;

    if (aEnd < bStart || bEnd < aStart) {
      return false;
    }

    return true;
  }
}

class Obstacle extends CircleObject {
  Obstacle({required this.round, required super.base, required super.angle, required super.height, required super.width});
  final int round;
}

const pi2 = math.pi * 2;
const gravity = 0.6;
const double heightBase = 160;
const double jumpVelicity = 6.4;

class Runner extends CircleObject {
  Runner({required super.base, required super.angle, required super.height, required super.width});

  var rounds = 0;
  double angularVelocity = math.pi / 100;
  double angularVelocityIncrement = math.pi / 1500;
  double? verticalSpeed;
  
  void update() {
    angle += angularVelocity + ((rounds ~/ 4) * angularVelocityIncrement);
    if (angle > pi2) {
      angle -= pi2;
      ++rounds;
    }

    var jumpSpeed = verticalSpeed;
    if (null != jumpSpeed) {
      jumpSpeed -= gravity;
      base += jumpSpeed;
      if (base < heightBase) {
        base = heightBase;
        jumpSpeed = null;
      }
      verticalSpeed = jumpSpeed;
    }
  }

  void jump() {
    if (null != verticalSpeed) return;
    verticalSpeed = jumpVelicity;
  }

}

class _GamePageState extends State<GamePage> {
  final _obstacles = <Obstacle>[];
  
  final _runner = Runner(
    base: heightBase,
    angle: 0,
    height: 10,
    width: 10
  );

  final rng = math.Random();
  
  int _score = 0;
  int _hiscore = 0;

  Timer? _timer;

  void _incrementAngle() {
      _runner.update();

      if (_obstacles.any((o) => o.intersects(_runner))) {
          _stopTimer();
          setState(() {});
          return;
      }

    final xAngle = math.atan((_runner.width / 2) / _runner.base);

    var ocount = _obstacles.length;
    _obstacles
      .removeWhere((o) => o.round < _runner.rounds || (o.round == _runner.rounds && ((_runner.angle - xAngle)) > ((o.angle + xAngle))));
    ocount -=_obstacles.length;
    
    _score += ocount;
    _hiscore = math.max(_hiscore, _score);

    final obstacleCount = math.min(1 + _score ~/ 3, 5);
    for (int i = _obstacles.length; i < obstacleCount; ++i) {
      final oangle = _runner.angle - 2 * xAngle - (pi2 * 0.5) * rng.nextDouble();
      _obstacles.add( Obstacle(
        round: _runner.rounds + (oangle < 0 ? 0 : 1),
        angle: oangle < 0 ? pi2 + oangle : oangle,
        base: heightBase,
        width: 10,
        height: 5 + rng.nextDouble() * 10
    ));

    }

    setState((){});
  }

  @override
  void initState() { 
    super.initState();
  }
  
  void _startTimer() {
    _stopTimer();
    _obstacles.clear();
    _runner.angle = 0;
    _runner.base = heightBase;
    _runner.verticalSpeed = null;
    _runner.rounds = 0;
    _score = 0;
    _timer = Timer.periodic(const Duration(milliseconds: 40), (timer) => _incrementAngle());
  }
  
  void _stopTimer() {
    _timer?.cancel();
    _timer = null;
    setState((){});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text('CircleJump Hiscore: $_hiscore'),
      ),
      body: 
      Listener(
        onPointerDown: (_) {
          if (null == _timer) { _startTimer(); }
          else { _runner.jump(); }
        },
      child: Focus(
        autofocus: true,
        onKey: (node, event) {
          switch(event) {
            case RawKeyDownEvent(logicalKey:LogicalKeyboardKey.escape, repeat:false):
              _stopTimer();
            case RawKeyDownEvent(logicalKey:LogicalKeyboardKey.space, repeat:false):
              if (null == _timer) { _startTimer(); }
              else { _runner.jump(); }
            case RawKeyUpEvent():
              // jump fly
          }
          return KeyEventResult.handled;
        },
      child: Center( 
        
      child: Stack(
          children: [
            Container(
              margin: const EdgeInsets.all(heightBase),
              width: 2 * heightBase,
              height: 2 * heightBase,
              decoration: const BoxDecoration(
                color: Colors.amber,
                shape: BoxShape.circle,
              ),
            ),
            Positioned(
              top: heightBase + heightBase - 5,
              left: heightBase + 2*heightBase - 10,
            child:
            Transform.rotate(angle: 0,
            child: Container(
              width: 10,
              height: 5,
              decoration: const BoxDecoration(
                color: Colors.orange,
                //shape: BoxShape.circle,
              ),
            ),
            ),
            ),
            ... _obstacles.map((o) =>
            Positioned(
              top: heightBase + heightBase - (o.width / 2) + o.y,
              left:  heightBase + heightBase - (o.height / 2) + o.x,
              child: Transform.rotate(
                angle: o.angle,
                child: Container(
                  width: o.height,
                  height: o.width,
                  decoration: const BoxDecoration(
                    color: Colors.grey,
                  ),
                ),
              ),
            )).toList(),
            Positioned(
              top: heightBase + heightBase - (_runner.width / 2) + _runner.y,
              left: heightBase + heightBase - (_runner.height / 2) + _runner.x,
            child:
            Transform.rotate(angle: _runner.angle,
            child: Container(
              width: _runner.width,
              height: _runner.height,
              decoration: const BoxDecoration(
                color: Colors.red,
                //shape: BoxShape.circle,
              ),
            ),
            ),
            ),
            Positioned.fill(child: 
              //top: 2 * heightBase,
              //left: 2* heightBase,
              Center(
            child: Text('$_score', style: const TextStyle(fontWeight: FontWeight.w900, fontSize: 30)),
              ),
            ),
            if (null == _timer) Positioned(
              left: heightBase,
              child: Text('Press space to start / jump, ESC to stop & restart'),
            )
          ],
        ),
      ),
      ),
      ),
    );
  }
}
