import 'dart:async';
import 'package:flutter/material.dart';
import 'missile_command.dart';
import 'missile_command_painter.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Missile Command',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.redAccent),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Missile Command'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
 final String title;
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final missileCommand = MissileCommand();
  Timer? timer;

  @override
  void initState() { 
    super.initState();
    timer = Timer.periodic(const Duration(milliseconds: 100), (timer) => setState(() => missileCommand.update()));
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        SizedBox.expand(
        child: CustomPaint(
          painter: MissilePainter(missileCommand),
          child: GestureDetector(
            onTapDown: (details) {
               final RenderBox renderBox = context.findRenderObject() as RenderBox;
               final x = missileCommand.width * (details.localPosition.dx / renderBox.size.width);
               final y = missileCommand.height * (details.localPosition.dy / renderBox.size.height);
               setState(() => missileCommand.fireAt(x, y));
            },
          ),
        ),
      ),
      if (missileCommand.gameOver || missileCommand.isLevelCompleted) ModalBarrier(
        dismissible: false,
        color: Colors.black.withOpacity(0.5),
      ),
      if (!missileCommand.gameOver && missileCommand.isLevelCompleted) Center(
        child: TextButton(
          onPressed: missileCommand.nextLevel,
          child: Text(
            0 == missileCommand.level ? 'Start' : 'Next level',
            style: const TextStyle(fontFamily: 'BebasNeue', fontSize: 30),
          ),
        ),
      ),
      if (missileCommand.gameOver) const Center(
        child: Text(
          'THE END',
          style: TextStyle(fontFamily: 'BebasNeue', fontSize: 80),
        ),
      ),
      ],
    );
  }
}
