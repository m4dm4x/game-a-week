import 'dart:math';
import 'dart:ui';

class Missile {
  Missile({required this.start, required this.speed}):position=start;
  Offset start;
  Offset position;
  bool exploded = false;
  final Offset speed;

  void update() { position += speed; }
}

class PlayerMissile extends Missile {
  PlayerMissile({required this.target, required super.start, required super.speed});
  Offset target;
  bool get onTarget => (target-position).distanceSquared < speed.distanceSquared;
}

class City {
  City({required this.position, required this.size});
  
  final Offset position;
  final Size size;
  bool destroyed = false;

  Offset get center => position + Offset(size.width / 2, size.height / 2); 

  double distanceSquaredTo(Offset position) =>
    (position - this.position).distanceSquared;
  
  bool hitTest(Missile missile) {
    return missile.position.dy > position.dy && position.dx < missile.position.dx && missile.position.dx < (position.dx + size.width);
  }
}

class PlayerSilo extends City {
  PlayerSilo({required super.position, required super.size}): missileCount = 10;
  int missileCount;
}

class MissileExplosion {
  MissileExplosion({required this.position}):radius=2,speed=2.2,maxRadius=45;
  Offset position;
  double radius;
  double speed;
  double maxRadius;
  void update() { if (radius < maxRadius) { radius += speed; } }
  bool get onTarget => radius >= maxRadius;

  bool inRadius(Missile missile) {
    final explosionDistance2 = (position - missile.position).distanceSquared;
    return (radius*radius) >= explosionDistance2;
  }
}

final cspace = (1024/2 - 40 - 300) / 4;
final cstart = 1024/2 + 10;

class MissileCommand {
  int playerScore = 0;
  int level = 0;
  int enemyMissilesCount = 0;
  final enemyMissiles = <Missile>[];
  final playerMissiles = <PlayerMissile>[];
  final playerExplosion = <MissileExplosion>[];
  final playerCity = <City>[
    City(position:Offset(30+(1*cspace)+(0*100), 700), size: Size(100,20)),
    City(position:Offset(30+(2*cspace)+(1*100), 700), size: Size(100,20)),
    City(position:Offset(30+(3*cspace)+(2*100), 700), size: Size(100,20)),
    City(position:Offset(cstart+(1*cspace)+(0*100), 700), size: Size(100,20)),
    City(position:Offset(cstart+(2*cspace)+(1*100), 700), size: Size(100,20)),
    City(position:Offset(cstart+(3*cspace)+(2*100), 700), size: Size(100,20))
  ];

  final playerSilo = <PlayerSilo>[
    PlayerSilo(position: Offset(10, 680), size: Size(20,20)),
    PlayerSilo(position: Offset(1024/2 - 10, 680), size: Size(20,20)),
    PlayerSilo(position: Offset(1024-20-10, 680), size: Size(20,20)), 
  ];

  final width = 1024;
  final height = 768;

  bool get gameOver => playerCity.isEmpty;

  bool get isLevelCompleted => enemyMissilesCount >= level && enemyMissiles.isEmpty;

  void fireAt(double x, double y) {
    final target = Offset(x, y);
    
    PlayerSilo? selectedSilo;
    
    for (final silo in playerSilo) {
      if (silo.missileCount < 1) {
        continue;
      }
      selectedSilo ??= silo;
      if (selectedSilo.distanceSquaredTo(target) > silo.distanceSquaredTo(target)) {
        selectedSilo = silo;
      }
    }

    if (null != selectedSilo) {
      var speed = target - selectedSilo.center;
      speed /= speed.distance / 6;
      playerMissiles.add(PlayerMissile(target: target, start: selectedSilo.center, speed: speed));
      --selectedSilo.missileCount;
    }
  }

  final rng = Random();

  void nextLevel() {
    if (!isLevelCompleted) { return; }
    level = gameOver ? 1 : (level + 1);
    enemyMissilesCount = 0;
    playerMissiles.clear();
    playerExplosion.clear();
    for (final silo in playerSilo) {
      playerScore += silo.missileCount * 10;
      silo.missileCount = 10;
    }
  }

  void update() {
    if (!gameOver && enemyMissilesCount < level && rng.nextInt(1000) < 10) {
      final start = Offset(rng.nextDouble()*width, -5);
      final targets = [...playerCity,...playerSilo];
      assert(targets.isNotEmpty);
      final target = targets[rng.nextInt(targets.length)].center;
      var speed = target - start;
      speed /= speed.distance / 2.5;
      final missile = Missile(start: start, speed: speed);
      enemyMissiles.add(missile);
      ++enemyMissilesCount;
    }

    for (final explosion in playerExplosion) {
      explosion.update();
    }

    final newExplosions = <MissileExplosion>[];

    for(final missile in enemyMissiles) {
      missile.update();
      if (missile.position.dy > 720) missile.exploded = true;
      for (final explosion in playerExplosion) {
        if (missile.exploded) {
          break;
        }
        if (!explosion.inRadius(missile)) {
          continue;
        }
        final mexplosion = MissileExplosion(position: missile.position);
        mexplosion.maxRadius = 30;
        newExplosions.add(mexplosion);
        missile.exploded = true;
        playerScore += 5;
      }
      if (missile.exploded) {
        continue;
      }
      for (final city in [...playerCity, ...playerSilo]) {
        if (city.hitTest(missile)) {
          newExplosions.add(MissileExplosion(position: city.center));
          missile.exploded = true;
          city.destroyed = true;
        }
      }
    }

    for(final missile in playerMissiles) {
      missile.update();
      if (missile.onTarget) {
        newExplosions.add(MissileExplosion(position: missile.target));
      }
    }
    
    playerCity.removeWhere((city) => city.destroyed);
    playerSilo.removeWhere((city) => city.destroyed);
    enemyMissiles.removeWhere((missile) => missile.exploded);
    playerMissiles.removeWhere((missile) => missile.onTarget);
    playerExplosion.removeWhere((explosion) => explosion.onTarget);
    playerExplosion.addAll(newExplosions);
  }
}
