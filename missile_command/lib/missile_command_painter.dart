import 'dart:ui';

import 'package:flutter/widgets.dart' as w;
import 'missile_command.dart';

class MissilePainter extends w.CustomPainter {
  MissilePainter(this.missileCommand);

  final MissileCommand missileCommand;

  @override
  bool? hitTest(Offset offset) {
    
    return null;
  }

  Offset _pos(Offset p, Size s) => Offset(s.width * (p.dx / missileCommand.width), s.height * (p.dy / missileCommand.height));

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawColor(Color.fromARGB(255, 31, 31, 31), BlendMode.src);

    final paintGround = Paint()
    ..strokeWidth = 2
    ..style = PaintingStyle.stroke
    ..color = Color.fromARGB(255, 166, 255, 0)
    ..maskFilter = MaskFilter.blur(BlurStyle.normal, 1.0)
    ;

    final ground = [
      Offset(0, 720), Offset(10, 700),
      Offset(10, 700), Offset(30, 700),
      Offset(30, 700), Offset(40, 720),
      Offset(40, 720), Offset(1024/2 - 20, 720),
      Offset(1024/2 - 20, 720), Offset(1024/2 - 10, 700),
      Offset(1024/2 - 10, 700), Offset(1024/2 + 10, 700),
      Offset(1024/2 + 10, 700), Offset(1024/2 + 20, 720),
      Offset(1024/2 + 20, 720), Offset(1024-40, 720),
      Offset(1024-40, 720), Offset(1024-30, 700),
      Offset(1024-30, 700), Offset(1024-10, 700),
      Offset(1024-10, 700), Offset(1024, 720),
      ];
    canvas.drawPoints(PointMode.lines, ground.map((e) => _pos(e, size)).toList(), paintGround);

    {
    final pb = ParagraphBuilder(ParagraphStyle(fontFamily: 'BebasNeue', fontSize: 35));
    pb.pushStyle(TextStyle(color: Color.fromARGB(255, 255, 255, 255)));
    pb.addText('${missileCommand.level}');
    final p = pb.build();
    p.layout(ParagraphConstraints(width: 400));
    canvas.drawParagraph(p, _pos(Offset(50, 726), size));
    }

    {
    final pb = ParagraphBuilder(ParagraphStyle(fontFamily: 'BebasNeue', fontSize: 35,));
    pb.pushStyle(TextStyle(color: Color.fromARGB(255, 255, 255, 255)));
    pb.addText(missileCommand.playerScore.toString().padLeft(5,'0'));
    final p = pb.build();
    p.layout(ParagraphConstraints(width: 350));
    canvas.drawParagraph(p, _pos(Offset(missileCommand.width - 100, 726), size));
    }

    final paintSilo = Paint()
    ..strokeWidth = 2
    ..style = PaintingStyle.stroke
    ..color = Color.fromARGB(255, 0, 255, 0)
    ..maskFilter = MaskFilter.blur(BlurStyle.normal, 1.0)
    ;

    final paintCity = Paint()
    ..strokeWidth = 2
    ..style = PaintingStyle.stroke
    ..color = Color.fromARGB(255, 14, 126, 255)
    ..maskFilter = MaskFilter.blur(BlurStyle.normal, 1.0)
    ;
    
    final paint = Paint()
    ..color = Color.fromARGB(255, 243, 167, 2)
    ..maskFilter = MaskFilter.blur(BlurStyle.normal, 3.0)
    ;
    final paintEnemyTrace = Paint()
    ..color = Color.fromARGB(255, 255, 88, 88)
    ..maskFilter = MaskFilter.blur(BlurStyle.normal, 1.0)
    ;
    for (final silo in missileCommand.playerSilo) {
      final p = _pos(silo.position, size);
      final s = _pos(Offset(silo.size.width, silo.size.height), size);
      canvas.drawRect(Rect.fromLTWH(p.dx, p.dy, s.dx, s.dy), paintSilo);
      for (int i = 0; i < silo.missileCount; ++i) {
        final p = _pos(Offset(silo.position.dx + 5, silo.position.dy + silo.size.height + 10 + i*6), size);
        final s = _pos(Offset(10, 2), size);
        canvas.drawRect(Rect.fromLTWH(p.dx, p.dy, s.dx, s.dy), silo.missileCount > 3 ? paintSilo : paintEnemyTrace);
      }
    }

    for (final pm in missileCommand.playerCity) {
      final p = _pos(pm.position, size);
      final s = _pos(Offset(pm.size.width, pm.size.height), size);
      canvas.drawRect(Rect.fromLTWH(p.dx, p.dy, s.dx, s.dy), paintCity);
    }


    final paintTrace = Paint()
    ..color = Color.fromARGB(255, 255, 230, 0)
    ..maskFilter = MaskFilter.blur(BlurStyle.normal, 1.0)
    ;
    final paintTarget = Paint()
    ..color = Color.fromARGB(255, 177, 172, 162)
    ..maskFilter = MaskFilter.blur(BlurStyle.normal, 1.0)
    ;

    final paintEnemyMissile = Paint()
    ..color = Color.fromARGB(255, 255, 15, 15)
    ..maskFilter = MaskFilter.blur(BlurStyle.normal, 3.0)
    ;


    for (final missile in missileCommand.enemyMissiles) {
      canvas.drawLine(_pos(missile.start, size), _pos(missile.position,size), paintEnemyTrace);
      canvas.drawCircle(_pos(missile.position, size), 5, paintEnemyMissile);
    }
    for (final missile in missileCommand.playerMissiles) {
      canvas.drawCircle(_pos(missile.target, size), 3, paintTarget);
      canvas.drawCircle(_pos(missile.position, size), 5, paint);
      canvas.drawLine(_pos(missile.start,size), _pos(missile.position,size), paintTrace);
    }

    final paintExplosion = Paint()
    ..style = PaintingStyle.stroke
    ..strokeWidth = 3
    ..color = Color.fromARGB(255, 255, 192, 150)
    ..maskFilter = MaskFilter.blur(BlurStyle.normal, 2.0)
    ;
    for (final e in missileCommand.playerExplosion) {
      canvas.drawCircle(_pos(e.position, size), e.radius, paintExplosion);
    }

    //print(size);
  }

  @override
  bool shouldRepaint(covariant w.CustomPainter oldDelegate) {
    return true;
  }

}
