import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sliding_puzzle/puzzle.dart';

const color = [
  Color(0xff0d1b2a),
  Color(0xff1b263b),
  Color(0xff415a77),
  Color(0xff778da9),
  Color(0xffe0e1dd)
];

const numberTileSize = 70.0;

void main() {
  runApp(const SlidingPuzzleApp());
}

class SlidingPuzzleApp extends StatelessWidget {
  const SlidingPuzzleApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '🧩 Sliding Puzzle',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(
            brightness: Brightness.dark, seedColor: color[0]),
        brightness: Brightness.dark,
        useMaterial3: true,
      ),
      home: const PuzzleHomePage(),
    );
  }
}

class PuzzleHomePage extends StatelessWidget {
  const PuzzleHomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: color[1],
        title: const Text('Sliding Puzzle'),
      ),
      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [color[1], color[0]])),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              OutlinedButton.icon(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (ctx) => const MyHomePage(
                                  puzzleSize: 3,
                                )));
                  },
                  icon: const Icon(Icons.play_circle),
                  label: const Text(
                    '8 Puzzle',
                    style: TextStyle(fontSize: 25),
                  )),
              const SizedBox(
                height: 10,
              ),
              OutlinedButton.icon(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (ctx) => const MyHomePage(
                                  puzzleSize: 4,
                                )));
                  },
                  icon: const Icon(Icons.play_circle),
                  label: const Text('15 Puzzle', style: TextStyle(fontSize: 25))),
            ],
          ),
        ),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.puzzleSize});
  final int puzzleSize;
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class NumberTile extends StatelessWidget {
  const NumberTile(this.number,
      {this.inPosition = false, this.onTap, super.key});

  final int number;
  final bool inPosition;
  final void Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5),
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          alignment: Alignment.center,
          width: numberTileSize - 10,
          height: numberTileSize - 10,
          decoration: BoxDecoration(
            border: inPosition ? Border.all(color: color[3], width: 4) : null,
            borderRadius: const BorderRadius.all(Radius.circular(6)),
            color: 0 < number ? color[1] : color[0],
          ),
          child: 0 < number
              ? Text(
                  number.toString(),
                  style: TextStyle(
                      color: color[4],
                      fontSize: 30,
                      fontWeight: FontWeight.w700),
                )
              : null,
        ),
      ),
    );
  }
}

class NumberData {
  const NumberData({required this.row, required this.col});
  final int row;
  final int col;
}

class ScalingBox extends StatelessWidget {
  const ScalingBox(
      {super.key,
      required this.child,
      required this.logicWidth,
      required this.logicHeight});

  final double logicWidth;
  final double logicHeight;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return SizedBox.expand(
        child: FittedBox(
            fit: BoxFit.contain,
            alignment: Alignment.center,
            child: SizedBox(
              width: logicWidth,
              height: logicHeight,
              child: child,
            )));
  }
}

class _MyHomePageState extends State<MyHomePage> {
  final rng = Random();
  SlidingPuzzle puzzle = SlidingPuzzle(3);
  var _moveCount = 0;

  void _moveTo(int r, int c) {
    final dx = r - puzzle.row;
    final dy = c - puzzle.col;
    final move =
        puzzle.moves().where((e) => e.dx == dx && e.dy == dy).firstOrNull;
    if (null != move) {
      _move(move);
    }
  }

  void _move(Move? move) {
    if (null == move) {
      return;
    }
    if (!puzzle.moves().contains(move)) {
      return;
    }
    setState(() {
      puzzle.move(move);
      ++_moveCount;
    });
  }

  @override
  void initState() {
    super.initState();

    puzzle = SlidingPuzzle(widget.puzzleSize);

    Move? lastMove;
    for (int i = 0; i < 1000; ++i) {
      final moves = puzzle.moves();
      while (true) {
        final move = moves.elementAt(rng.nextInt(moves.length));
        if (null == lastMove || move != lastMove.inverse) {
          puzzle.move(move);
          lastMove = move;
          break;
        }
      }
    }
  }

  void _solvePuzzle() {
    _solutionStep( puzzleSolve(puzzle).reversed.iterator );
  }

  void _solutionStep(Iterator<List<int>> isolution) {
    if (!isolution.moveNext()) { return; }
    setState(() {
      puzzle.setData(isolution.current);
      ++_moveCount;
    });
    Future.delayed(const Duration(milliseconds: 300)).whenComplete(() => _solutionStep(isolution));
  }

  @override
  Widget build(BuildContext context) {
    final numbers = <NumberData>[];
    for (int r = 0; r < puzzle.n; ++r) {
      for (int c = 0; c < puzzle.n; ++c) {
        numbers.add(NumberData(row: r, col: c));
      }
    }
    return Scaffold(
      appBar: AppBar(
          backgroundColor: color[1],
          title: Center(
            child: Text(
              '⇄\t$_moveCount',
              style: TextStyle(color: color[4]),
            ),
          ),
          actions: [
            TextButton.icon(onPressed: _solvePuzzle, icon: Icon(Icons.onetwothree_rounded), label: Text('Solve'))
          ],
          ),
      body: Focus(
        autofocus: true,
        onKey: (node, event) {
          switch (event) {
            case RawKeyDownEvent(logicalKey: LogicalKeyboardKey.arrowUp):
              _move(Move.up);
            case RawKeyDownEvent(logicalKey: LogicalKeyboardKey.arrowDown):
              _move(Move.down);
            case RawKeyDownEvent(logicalKey: LogicalKeyboardKey.arrowLeft):
              _move(Move.left);
            case RawKeyDownEvent(logicalKey: LogicalKeyboardKey.arrowRight):
              _move(Move.right);
          }
          return KeyEventResult.handled;
        },
        child: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [color[1], color[0]])),
          child: Center(
            child: 
              ScalingBox(
              logicHeight: numberTileSize * puzzle.n,
              logicWidth: numberTileSize * puzzle.n,
            child: Container(
              decoration: BoxDecoration(
                color: color[2],
                borderRadius: BorderRadius.circular(6),
              ),
              child: Stack(children: [
                ...numbers
                    .map((e) => AnimatedPositioned(
                          key: ValueKey(puzzle.get(e.row, e.col)),
                          top: numberTileSize * e.row,
                          left: numberTileSize * e.col,
                          duration: const Duration(milliseconds: 300),
                          child: NumberTile(
                            puzzle.get(e.row, e.col),
                            onTap: () => _moveTo(e.row, e.col),
                            inPosition: puzzle.isPlaced(e.row, e.col),
                          ),
                        ))
                    .toList(),
                if (puzzle.isSolved)
                  ModalBarrier(color: color[1].withOpacity(0.5)),
                if (puzzle.isSolved)
                  Center(
                      child: Text('🏆 Solved!',
                          style: TextStyle(
                              fontSize: 50,
                              color: color[4],
                              fontWeight: FontWeight.w900))),
              ]),
            ),
              ),
          ),
        ),
      ),
    );
  }
}
