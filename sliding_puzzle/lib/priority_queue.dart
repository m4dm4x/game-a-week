class PriorityQueue<T> {
  final data = <(int, T)>[];

  int get length => data.length;
  bool get isEmpty => data.isEmpty;
  bool get isNotEmpty => data.isNotEmpty;

  int get topPriority => data.first.$1;
  T get topValue => data.first.$2;

  T take() {
    final value = topValue;
    _swap(0, length - 1);
    data.removeLast();
    _bubbleDown(0);
    return value;
  }

  void insert(int priority, T value) {
    data.add((priority, value));
    _bubbleUp(length - 1);
  }

  void _bubbleUp(int index) {
    while (index > 0) {
      final par = _parent(index);
      if (data[par].$1 <= data[index].$1) {
        break;
      }
      _swap(index, par);
      index = par;
    }
  }

  void _bubbleDown(int index) {
    while (index < length) {
      var left = _left(index);
      if (left >= length) {
        return;
      }

      var right = _right(index);
      if (right >= length) right = left;

      if (data[right].$1 < data[left].$1) {
        left = right;
      }

      if (data[left].$1 >= data[index].$1) {
        break;
      }

      _swap(left, index);
      index = left;
    }
  }

  void _swap(int a, int b) {
    final temp = data[a];
    data[a] = data[b];
    data[b] = temp;
  }

  static int _parent(int i) => ((i + 1) ~/ 2) - 1;
  static int _right(int i) => 2 * (i + 1);
  static int _left(int i) => 2 * (i + 1) - 1;
}
