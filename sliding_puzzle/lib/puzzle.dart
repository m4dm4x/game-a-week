import 'dart:collection';

import 'priority_queue.dart';

enum Move {
  up(-1, 0),
  down(1, 0),
  left(0, -1),
  right(0, 1);

  const Move(this.dx, this.dy);

  final int dx;
  final int dy;

  Move get inverse => switch (this) {
        Move.left => Move.right,
        Move.right => Move.left,
        Move.down => Move.up,
        Move.up => Move.down
      };
}

class PuzzleData {
  const PuzzleData(
      {required this.n,
      required this.row,
      required this.col,
      required this.list});

  final int n;
  final int row;
  final int col;
  final List<int> list;
}

int _nValue(int idx, int n) => (idx+1) < (n*n) ? (idx+1) : 0;

class SlidingPuzzle {
  SlidingPuzzle(this.n)
      : data = List<int>.generate(n * n, (idx) => _nValue(idx, n)),
        row = n - 1,
        col = n - 1 {
    if (n < 2) {
      throw ArgumentError.value(n);
    }
  }

  final int n;
  int row;
  int col;
  List<int> data;

  int _value(int index) => _nValue(index, n);

  bool isPlaced(int r, int c) => data[_i(r, c)] == _value(_i(r, c));

  bool get isSolved {
    for (int i = 0; i < data.length; ++i) {
      if (data[i] != _value(i)) {
        return false;
      }
    }
    return true;
  }

  void shuffle() {
    data.shuffle();
    _updateIndex();
  }

  void _updateIndex() {
    final idx = data.indexOf(0);
    assert(idx >= 0);
    row = idx ~/ n;
    col = idx % n;
  }

  int get(int r, int c) => data[_i(r, c)];
  void _set(int r, int c, int v) => data[_i(r, c)] = v;

  List<Move> moves() {
    final ml = <Move>[];
    if (0 < row) ml.add(Move.up);
    if (row < (n - 1)) ml.add(Move.down);
    if (0 < col) ml.add(Move.left);
    if (col < (n - 1)) ml.add(Move.right);
    return ml;
  }

  void move(Move move) {
    _swap(row, col, row + move.dx, col + move.dy);
    row += move.dx;
    col += move.dy;
  }

  void _swap(int r1, int c1, int r2, int c2) {
    final temp = get(r1, c1);
    _set(r1, c1, get(r2, c2));
    _set(r2, c2, temp);
  }

  String toString() {
    final out = StringBuffer();
    out.write('[');
    for (int r = 0; r < n; ++r) {
      out.write('[');
      for (int c = 0; c < n; ++c) {
        out.write(get(r, c));
        out.write(',');
      }
      out.write(']');
    }
    out.write(']');

    return out.toString();
  }

  void setData(List<int> data) {
    this.data = data;
    _updateIndex();
  }

  List<int> getData() => data.toList();

  int _i(int r, int c) => c + r * n;
}

bool listEquals(List<int> a, List<int> b) {
  if (a.length != b.length) {
    return false;
  }
  for (int i = 0; i < a.length; ++i) {
    if (a[i] != b[i]) {
      return false;
    }
  }
  return true;
}

int listHashCode(List<int> list) => Object.hashAll(list);

int hammingDistance(int n, List<int> list) {
  assert(n * n == list.length);
  int distance = 0;
  for (int i = 0; i < list.length; ++i) {
    if (_nValue(i, n) != list[i]) {
      ++distance;
    }
  }
  return distance;
}

int manhatanDistance(int n, List<int> list) {
  assert(n * n == list.length);
  int distance = 0;
  var i = 0;
  for (int r = 0; r < n; ++r) {
    for (int c = 0; c < n; ++c) {
      if (list[i] != _nValue(i, n)) {
        final x = (list[i] != 0 ? list[i] : n * n) - 1;
        final ir = x ~/ n;
        final ic = x % n;
        distance += (r - ir).abs() + (c - ic).abs();
      }
      ++i;
    }
  }
  return distance;
}

int inversionCount(List<int> list) {
  int inversions = 0;
  for (int i = 0; i < (list.length - 1); ++i) {
    if (0 == list[i]) continue;
    for (int j = i + 1; j < list.length; ++j) {
      if (0 == list[j]) continue;
      if (list[i] > list[j]) {
        ++inversions;
      }
    }
  }
  return inversions;
}

bool hasSolution(SlidingPuzzle puzzle) {
  final inversions = inversionCount(puzzle.data);
  if (puzzle.n.isOdd) {
    return inversions.isEven;
  } else {
    return (inversions + puzzle.row).isOdd;
  }
}

List<List<int>> puzzleSolve(SlidingPuzzle puzzle) {
  const distanceFunction = manhatanDistance;

  final knownState =
      HashMap<List<int>, List<int>>(equals: listEquals, hashCode: listHashCode);
  knownState[puzzle.getData()] = [];

  final pq = PriorityQueue<(int, List<int>)>();
  pq.insert(distanceFunction(puzzle.n, puzzle.data), (0, puzzle.getData()));

  while (pq.isNotEmpty && 0 < pq.topPriority) {
    var (steps, data) = pq.take();
    puzzle.setData(data.toList());
    final cost = 1 + steps;
    for (final move in puzzle.moves()) {
      puzzle.move(move);
      if (!knownState.containsKey(puzzle.data)) {
        final distance = distanceFunction(puzzle.n, puzzle.data);
        knownState[puzzle.getData()] = data;
        pq.insert((0 == distance ? 0 : cost) + 2 * distance,
            (cost, puzzle.getData()));
      }
      puzzle.move(move.inverse);
    }
  }

  final solution = <List<int>>[];

  if (pq.isNotEmpty) {
    var state = pq.topValue.$2;
    while (state.isNotEmpty) {
      solution.add(state);
      state = knownState[state] ?? [];
    }
  }

  return solution;
}

void main(List<String> arguments) {
  final matrix = SlidingPuzzle(4);
  //matrix.setData([0,1,3,4,2,5,7,8,6]);

  do {
    matrix.shuffle();
  } while (!hasSolution(matrix));

  print(matrix);

  for (final step in puzzleSolve(matrix).reversed) {
    print(step);
  }
}
