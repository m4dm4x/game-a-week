import 'dart:math';

import 'package:flutter/material.dart';
import 'sudoku.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.blue),
        useMaterial3: true,
      ),
      home: const SudokuPage(title: 'Sudoku'),
    );
  }
}

class BorderMajor extends BorderSide {
  const BorderMajor():super(color: Colors.black, width: 4);
}

class BorderMinor extends BorderSide {
  const BorderMinor():super(color: Colors.grey, width: 2);
}

class SudokuPage extends StatefulWidget {
  const SudokuPage({super.key, required this.title});

  final String title;

  @override
  State<SudokuPage> createState() => _SudokuPageState();
}

class Matrix {
  Matrix(this.rowCount, this.columnCount):list = List<int>.filled(rowCount*columnCount, 0);
  final int rowCount;
  final int columnCount;
  final List<int> list;

  int get(int row, int col) => list[col + row * columnCount];
  void set(int row, int col, int val) => list[col + row * columnCount] = val;

  void clear() { for (int i = 0; i < list.length; ++i) { list[i] = 0; } }
}

class _SudokuPageState extends State<SudokuPage> {
  final rng = Random();
  Sudoku sudoku = Sudoku(3);
  Sudoku solution = Sudoku(3);
  final matrix = Matrix(9, 9);
  int rowSelected = 0;
  int colSelected = 0;

  bool get isNotBase => 0 == sudoku.get(rowSelected, colSelected);
  bool get canDelete => isNotBase && 0 != matrix.get(rowSelected, colSelected);
  bool canWrite(int v) => isNotBase && (0 == matrix.get(rowSelected, colSelected) || v != matrix.get(rowSelected, colSelected));
  
  bool canGenerateSudoku() {
    if (solution.solved) { return true; }
    for(int r = 0; r < sudoku.rowCount; ++r) {
      for(int c = 0; c < sudoku.columnCount; ++c) {
        if (0 != sudoku.get(r, c)) { return false; }
      }
    }
    return true;
  }

  void generateSudoku() {
    sudoku = generate(rng, 3);
    solution = Sudoku(3);
    for(int r = 0; r < sudoku.rowCount; ++r) {
      for(int c = 0; c < sudoku.columnCount; ++c) {
        matrix.set(r, c, sudoku.get(r, c));
        solution.set(r, c, sudoku.get(r, c));
      }
    }
    setState(() {});
  }

  void setValue(int v) {
    setState(() {
      matrix.set(rowSelected,colSelected,v);
      solution.unset(rowSelected, colSelected);
      solution.set(rowSelected, colSelected, v);
    });
  }

  @override
  void initState() { 
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          if (solution.solved) Text('Solved!',style: TextStyle(fontSize: 40, fontWeight: FontWeight.w500)),
      SizedBox(height: 48.0*matrix.rowCount,width: 48.0*matrix.columnCount,child:
      GridView.count(crossAxisCount: 9, 
      children: List<Widget>.generate(matrix.rowCount * matrix.columnCount, (idx) {
        final row = idx ~/ matrix.columnCount;
        final col = idx % matrix.columnCount;
        final baseValue = sudoku.get(row, col);
        final isBase = 0 != baseValue;
        final value = isBase ? baseValue : matrix.get(row,col);
        final isEmpty = 0 == value;
      return GestureDetector(
        onTap: () {setState(() {
              rowSelected = row;
              colSelected = col;
            });
        },
        child: Container(
        decoration: BoxDecoration(
                border: Border(
                  top: 0 == (idx ~/ 9) % 3 ? BorderMajor() : BorderMinor(),
                  left: 0 == idx % 3 ? BorderMajor() : BorderMinor(),
                  right: 8 == idx % 9 ? BorderMajor() : BorderSide.none,
                  bottom: 8 == idx ~/ 9 ? BorderMajor() : BorderSide.none,
                ),
                color: (rowSelected == row && colSelected == col) ? Colors.grey.shade400 :
                (rowSelected == row || colSelected == col) ? Colors.grey.shade200 : null,
          ),
          child: Text(isEmpty ? '' : '$value', style: TextStyle(fontSize: 32, fontWeight: isBase ? FontWeight.w900 : null), textAlign: TextAlign.center,),
      ));}).toList(),
      ),
      ),
  if (!canGenerateSudoku()) Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      ...List.generate(9, (n) => OutlinedButton(onPressed: !canWrite(n+1) ? null : () => setValue(n+1), child: Text('${1+n}', style: TextStyle(fontSize: 32, fontWeight: FontWeight.w900), textAlign: TextAlign.center,)),).toList(),
      OutlinedButton(onPressed: !canDelete ? null : () => setState(() { matrix.set(rowSelected,colSelected,0);}) , child: Padding( padding: EdgeInsets.only(top: 6, bottom: 6), child: Icon(Icons.delete, size: 32,)))
    ],
  ),
  if (canGenerateSudoku()) Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [OutlinedButton(child: Text('New Sudoku!'), onPressed: () => generateSudoku(),)])
  
      ]),      
    );
  }
}
