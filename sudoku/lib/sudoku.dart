import 'dart:math' as math;

int _pow2(int num) => num*num;

class Sudoku {
  Sudoku(this.boxSize) : 
    _data = List.filled(_pow2(_pow2(boxSize)), 0),
    _rowBitmap = List.filled(_pow2(boxSize), 0),
    _colBitmap = List.filled(_pow2(boxSize), 0),
    _boxBitmap = List.filled(_pow2(boxSize), 0);

  factory Sudoku.fromString(String values) {
    final boxSize = math.sqrt(math.sqrt(values.length)).toInt();
    if (_pow2(_pow2(boxSize)) != values.length) {
      throw ArgumentError('Invalid length');
    }
    final sudoku = Sudoku(boxSize);
    for (int row = 0; row < sudoku.rowCount; ++row) {
      for (int col = 0; col < sudoku.columnCount; ++col) {
        final idx = col + row * sudoku.columnCount;
        switch(values[idx]) {
          case '1': sudoku.set(row, col, 1);
          case '2': sudoku.set(row, col, 2);
          case '3': sudoku.set(row, col, 3);
          case '4': sudoku.set(row, col, 4);
          case '5': sudoku.set(row, col, 5);
          case '6': sudoku.set(row, col, 6);
          case '7': sudoku.set(row, col, 7);
          case '8': sudoku.set(row, col, 8);
          case '9': sudoku.set(row, col, 9);
        }
      }
    }
    return sudoku;
  }

  final int boxSize;
  List<int> _data;
  List<int> _rowBitmap;
  List<int> _colBitmap;
  List<int> _boxBitmap;

  final undoBuffer = <(int,int)>[];

  int get maxValue => _pow2(boxSize);
  int get columnCount => _pow2(boxSize);
  int get rowCount => columnCount;

  bool get solved => !_data.any((element) => 0 == element);

  int get(int row, int col) => _data[_i(row,col)];

  int bitmask(int row, int col) {
    return _rowBitmap[row] | _colBitmap[col] | _boxBitmap[_b(row, col)];
  }

  int get undoSize => undoBuffer.length;

  void undo() {
    if (undoBuffer.isEmpty) { return;}
    final (int row, int col) = undoBuffer.removeLast();
    unset(row, col);
  }

  void unset(int row, int col) {
    final value = get(row,col);
    if (0 == value) { return; }
    final valueBitmask = 1 << value;
    _rowBitmap[row] &= ~valueBitmask;
    _colBitmap[col] &= ~valueBitmask;
    _boxBitmap[_b(row, col)] &= ~valueBitmask;
    _data[_i(row,col)] = 0;
  }

  bool set(int row, int col, int value) {
    assert(row < rowCount);
    assert(col < columnCount);
    assert(value <=maxValue);
    if (0 != get(row,col)) {
      return false;
    }
    final valueBitmask = 1 << value;
    if (valueBitmask == valueBitmask & bitmask(row, col)) {
      return false;
    }
    _rowBitmap[row] |= valueBitmask;
    _colBitmap[col] |= valueBitmask;
    _boxBitmap[_b(row, col)] |= valueBitmask;
    _data[_i(row,col)] = value;
    undoBuffer.add((row,col));
    return true;
  }
/*
  void print() {
    for (int r = 0; r < rowCount; ++r) {
      for (int c = 0; c < columnCount; ++c) {
        stdout.write('${get(r,c)} ');
      }
    stdout.write('\n');
    }
  }
*/
  String toString() {
    final out = StringBuffer();
    for (final i in _data) {
      out.write(0 < i ? i.toString() : '.');
    }
    return out.toString();
  }

  int _i(int row, int col) => col + row * columnCount;
  int _b(int row, int col) => (col ~/ boxSize) + boxSize * (row ~/ boxSize);
}

int valueFromMask(int mask, int maxValue) {
  for (int value = 1; value <= maxValue; ++value) {
    if ((1 << value) == mask) {
      return value;
    }
  }
  return 0;
}

bool rowSolver(Sudoku sudoku) {
  bool changed = false;
  for (int col = 0; col < sudoku.columnCount; ++col) { 
    for (int row = 0; row < sudoku.rowCount; ++row) {
      if (0 != sudoku.get(row, col)) { continue; }
      var mask = ~sudoku.bitmask(row, col);
      for (int xrow = 0; xrow < sudoku.rowCount; ++xrow) {
        if (xrow == row) { continue; }
        if (0 != sudoku.get(xrow, col)) { continue; }
        mask &= sudoku.bitmask(xrow, col);
      }
      final value = valueFromMask(mask, sudoku.maxValue);
      if (value > 0) {
        sudoku.set(row, col, value);
        changed = true;
      }
    }
  }
  return changed;
}

bool colSolver(Sudoku sudoku) {
  bool changed = false;
  for (int row = 0; row < sudoku.rowCount; ++row) {
    for (int col = 0; col < sudoku.columnCount; ++col) { 
      if (0 != sudoku.get(row, col)) { continue; }
      var mask = ~sudoku.bitmask(row, col);
      for (int xcol = 0; xcol < sudoku.columnCount; ++xcol) { 
        if (xcol == col) { continue; }
        if (0 != sudoku.get(row, xcol)) { continue; }
        mask &= sudoku.bitmask(row, xcol);
      }
      final value = valueFromMask(mask, sudoku.maxValue);
      if (value > 0) {
        sudoku.set(row, col, value);
        changed = true;
      }
    }
  }
  return changed;
}

bool boxSolver(Sudoku sudoku) {
  bool changed = false;
  for (int row = 0; row < sudoku.rowCount; ++row) {
    for (int col = 0; col < sudoku.columnCount; ++col) {
      if (0 != sudoku.get(row, col)) { continue; }
      var mask = ~sudoku.bitmask(row, col);
      int rbase = sudoku.boxSize * (row ~/ sudoku.boxSize);
      int cbase = sudoku.boxSize * (col ~/ sudoku.boxSize);
      for (int rd = 0; rd < sudoku.boxSize; ++rd) {
        for (int cd = 0; cd < sudoku.boxSize; ++cd) {
          int rx = rbase + rd;
          int cx = cbase + cd;
          if (row == rx && col == cx) continue;
          if (0 != sudoku.get(rx, cx)) continue;
          mask &= sudoku.bitmask(rx, cx);
        }
      }
      final value = valueFromMask(mask, sudoku.maxValue);
      if (value > 0) {
        sudoku.set(row, col, value);
        changed = true;
      }
    }
  }
  return changed;
}


int backtrackSolver(Sudoku sudoku, [int row = 0, int col = 0, bool proveUnique = false]) {
  while (
    rowSolver(sudoku) ||
    colSolver(sudoku) ||
    boxSolver(sudoku)
  ) { continue; }

  if (sudoku.solved) {
    return 1;
  }

  int solutions = 0;
  for (; row < sudoku.rowCount; ++row) {
    for (; col < sudoku.columnCount; ++col) {
      if (0 != sudoku.get(row,col)) { continue; }
      for (int value = 1; value <= sudoku.maxValue ; ++value) {
        final num = sudoku.undoSize;
        if (!sudoku.set(row, col, value)) { continue; }
        solutions += backtrackSolver(sudoku, row, col+1);
        if ((solutions > 0 && !proveUnique) || solutions > 1) { return solutions; }
        while (num < sudoku.undoSize) {
          sudoku.undo();
        }
      }
      return solutions;
    }
    col = 0;
  }

  return solutions;
}

Sudoku generate(math.Random rng, int box) {
  final s = Sudoku(box);
  int u;
  do {
    while(s.undoSize > 0) { s.undo(); }
    bool noSolution = false;
    for (int i = 0; !noSolution && i < 30; ++i) {
      int row;
      int col;
      do {
        row = rng.nextInt(s.rowCount);
        col = rng.nextInt(s.columnCount);
      } while (0 != s.get(row,col));
    
      if (0 == (0x3FE & ~s.bitmask(row,col))) {
        noSolution = true;
        break;
      }
      int v;
      do {
        v = 1 + rng.nextInt(s.maxValue);
      } while(!s.set(row,col,v));
    }
    u = s.undoSize;
  } while (1 != backtrackSolver(s,0,0,false));
//  print(s);
  while(s.undoSize > u) { s.undo(); }
  return s;
}
